package ge.edu.btu.mvvm.ui.components

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ge.edu.btu.mvvm.App


object DataBindingComponents {
    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: ImageView, imageUrl: String) {
        Glide
            .with(App.instance.getContext())
            .load(imageUrl)
            .centerCrop()
            .into(view)
    }
}