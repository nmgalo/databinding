package ge.edu.btu.mvvm.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ge.edu.btu.mvvm.R
import ge.edu.btu.mvvm.databinding.NewsItemBinding
import ge.edu.btu.mvvm.ui.model.NewsModel

class NewsRecyclerViewAdapter(
    private val items: ArrayList<NewsModel>
) :
    RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: NewsItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.news_item,
            parent,
            false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

    override fun getItemCount() = items.size


    inner class ViewHolder(private val binding: NewsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.newsModel = items[adapterPosition]
        }
    }
}
