package ge.edu.btu.mvvm.data

interface FutureCallBack<T> {
    fun done(result: String) {}
    fun error(title: String, errorMessage: String) {}
}