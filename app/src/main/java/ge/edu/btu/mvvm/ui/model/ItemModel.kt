package ge.edu.btu.mvvm.ui.model


class ItemModel(
    val title: String,
    val image: String
)