package ge.edu.btu.mvvm.ui.activities

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import ge.edu.btu.mvvm.R
import ge.edu.btu.mvvm.data.ApiMethods
import ge.edu.btu.mvvm.data.DataLoader
import ge.edu.btu.mvvm.data.FutureCallBack
import ge.edu.btu.mvvm.ui.adapters.NewsRecyclerViewAdapter
import ge.edu.btu.mvvm.ui.model.NewsModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: NewsRecyclerViewAdapter
    private val news = ArrayList<NewsModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        setData()
        newsRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = NewsRecyclerViewAdapter(news)
        newsRecyclerView.adapter = adapter
    }


    private fun setData() {

        DataLoader.getRequest(
            null,
            ApiMethods.NEWS,
            object : FutureCallBack<String> {
                override fun done(result: String) {
                    val serverNewsResponse = Gson().fromJson(result, Array<NewsModel>::class.java)
                    news.addAll(serverNewsResponse)
                    adapter.notifyDataSetChanged()
                }

                override fun error(title: String, errorMessage: String) {
                    d("serverResponse", "$title $errorMessage")
                }
            }
        )
    }


}